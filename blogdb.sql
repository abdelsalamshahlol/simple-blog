-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2017 at 04:25 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blogdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `date_written` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `username`, `title`, `date_written`, `comment`) VALUES
(1, 5, 1, 'test', 'How to set Look and Feel in Groovy (SwingBuilder)?', '2017-04-26 03:29:15', '<p>LOOOOP TEST</p>'),
(2, 5, 1, 'test', 'How to set Look and Feel in Groovy (SwingBuilder)?', '2017-04-26 03:29:15', '<p>LOOOOP TEST</p>'),
(3, 5, 1, 'test', 'How to set Look and Feel in Groovy (SwingBuilder)?', '2017-04-26 03:29:15', '<p>LOOOOP TEST</p>'),
(4, 5, 1, 'test', 'Hello World', '2017-04-26 03:25:16', '<p>TEST NUMfbs</p><p>&nbsp;</p>'),
(5, 4, 1, 'test', 'Dummy TEXT TEST', '2017-04-26 07:55:22', '<p><em><strong>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget,</strong></em> arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet <strong>imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia C</strong>urae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero.</p><ul><li>Phasellus dolor. Maecenas vestibulum mollis</li><li><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere <em>imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, tu</em>rpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p></li></ul>'),
(6, 9, 1, 'abdulsalam', 'Hello World', '2017-04-26 08:14:32', '<p>Thanks for sharing</p>');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT 'Blog Post',
  `text` mediumtext NOT NULL,
  `datepublished` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `username`, `title`, `text`, `datepublished`, `user_id`) VALUES
(2, 'test', 'Hello World', '<ol><li>THIS A <em>TEST </em>SO BEAR <a href="http://with.com">WITH</a> ME MR. <strong>DEVELOPER</strong></li></ol>', '2017-04-25 13:18:01', 1),
(3, 'test', 'How to set Look and Feel in Groovy (SwingBuilder)?', '<p><a href="https://www.codeigniter.com/user_guide/libraries/sessions.html#id6">A note about concurrency</a></p><p>Unless you&rsquo;re developing a website with heavy AJAX usage, you can skip this section. If you are, however, and if you&rsquo;re experien', '2017-04-25 13:25:03', 1),
(4, 'test', 'Codeigniter Docs', '<p><a href="https://www.codeigniter.com/user_guide/libraries/sessions.html#id7">What is Session Data?</a></p><p>Session data is simply an array associated with a particular session ID (cookie).</p><p>If you&rsquo;ve used sessions in PHP before, you should be familiar with PHP&rsquo;s&nbsp;<a href="https://php.net/manual/en/reserved.variables.session.php">$_SESSION superglobal</a>&nbsp;(if not, please read the content on that link).</p><p>CodeIgniter gives access to its session data through the same means, as it uses the session handlers&rsquo; mechanism provided by PHP. Using session data is as simple as manipulating (read, set and unset values) the&nbsp;$_SESSION&nbsp;array.</p><p>In addition, CodeIgniter also provides 2 special types of session data that are further explained below: flashdata and tempdata.</p><p>Note</p><p>In previous versions, regular session data in CodeIgniter was referred to as &lsquo;userdata&rsquo;. Have this in mind if that term is used elsewhere in the manual. Most of it is written to explain how the custom &lsquo;userdata&rsquo; methods work.</p><p><a href="https://www.codeigniter.com/user_guide/libraries/sessions.html#id8">Retrieving Session Data</a></p><p>Any piece of information from the session array is available through the&nbsp;$_SESSION&nbsp;superglobal:</p><p>$_SESSION[&#39;item&#39;]</p><p>Or through the magic getter:</p><p>$this-&gt;session-&gt;item</p><p>And for backwards compatibility, through the&nbsp;userdata()&nbsp;method:</p><p>$this-&gt;session-&gt;userdata(&#39;item&#39;);</p><p>Where item is the array key corresponding to the item you wish to fetch. For example, to assign a previously stored &lsquo;name&rsquo; item to the&nbsp;$name&nbsp;variable, you will do this:</p><p>$name = $_SESSION[&#39;name&#39;]; // or: $name = $this-&gt;session-&gt;name // or: $name = $this-&gt;session-&gt;userdata(&#39;name&#39;);</p><p>Note</p><p>The&nbsp;userdata()&nbsp;method returns NULL if the item you are trying to access does not exist.</p><p>If you want to retrieve all of the existing userdata, you can simply omit the item key (magic getter only works for properties):</p><p>$_SESSION // or: $this-&gt;session-&gt;userdata();</p>', '2017-04-25 13:30:01', 1),
(5, 'test', 'YoGO', '<p>&#39;m investigating memory leak problems in a C++ application running on an embedded linux system (Yocto 1.5) with an ARM CPU.</p><p>Valgrind 3.8.1 is installed on the target.</p><p>The C++ program is compiled with gcc 4.8 with -g and -Og and is not stripped.</p><p>I&#39;ve launched valgrind with the command below:</p><p>$ valgrind --tool=memcheck --leak-check=full /tmp/e3event-daemon -c /etc/e3event-daemon/config.json</p><p>Output of valgrind:</p><p>==7035== Memcheck, a memory error detector ==7035== Copyright (C) 2002-2012, and GNU GPL&#39;d, by Julian Seward et al. ==7035== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info ==7035== Command: /tmp/e3event-daemon -c /etc/e3event-daemon/config.json ==7035== ==7035== ==7035== HEAP SUMMARY: ==7035== in use at exit: 4 bytes in 1 blocks ==7035== total heap usage: 421 allocs, 420 frees, 148,246 bytes allocated ==7035== ==7035== 4 bytes in 1 blocks are definitely lost in loss record 1 of 1 ==7035== at 0x4834558: operator new(unsigned int) (in /usr/lib/valgrind/vgpreload_memcheck-arm-linux.so) ==7035== ==7035== LEAK SUMMARY: ==7035== definitely lost: 4 bytes in 1 blocks ==7035== indirectly lost: 0 bytes in 0 blocks ==7035== possibly lost: 0 bytes in 0 blocks ==7035== still reachable: 0 bytes in 0 blocks ==7035== suppressed: 0 bytes in 0 blocks ==7035== ==7035== For counts of detected and suppressed errors, rerun with: -v ==7035== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 0 from 0)</p><p>A memory leak is reported but a&nbsp;<em>by 0x8048691: main (in ...</em>&nbsp;line is missing. If I run the same program on my Ubuntu Linux machine (valgrind 3.10</p>', '2017-04-25 13:55:22', 1),
(6, 'test', 'Hello World', '<p>HELLO</p>', '2017-04-26 03:17:10', 1),
(7, 'test', 'Hello World', '<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia. Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum nunc. Nunc nonummy metus. Vestibulum volutpat pretium libero. Cras id dui. Aenean ut eros et nisl sagittis vestibulum. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Sed lectus. Donec mollis hendrerit risus. Phasellus nec sem in justo pellentesque facilisis. Etiam imperdiet imperdiet orci. Nunc nec neque. Phasellus leo dolor, tempus non, auctor et, hendrerit quis, nisi. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae tortor. Donec posuere vulputate arcu. Phasellus accumsan cursus velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Phasellus consectetuer vestibulum elit. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Vestibulum fringilla pede sit amet augue. In turpis. Pellentesque posuere. Praesent turpis. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Nullam sagittis. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Ut varius tincidunt libero. Phasellus dolor. Maecenas vestibulum mollis</p>', '2017-04-26 07:55:46', 1),
(8, 'test', 'JQuery CSS Class', '<p>The&nbsp;.css()&nbsp;method is a convenient way to get a computed style property from the first matched element, especially in light of the different ways browsers access most of those properties (the&nbsp;getComputedStyle()&nbsp;method in standards-based browsers versus the&nbsp;currentStyle&nbsp;and&nbsp;runtimeStyle&nbsp;properties in Internet Explorer prior to version 9) and the different terms browsers use for certain properties. For example, Internet Explorer&#39;s DOM implementation refers to the&nbsp;float&nbsp;property as&nbsp;styleFloat, while W3C standards-compliant browsers refer to it as&nbsp;cssFloat. For consistency, you can simply use&nbsp;&quot;float&quot;, and jQuery will translate it to the correct value for each browser.</p><p>Also, jQuery can equally interpret the CSS and DOM formatting of multiple-word properties. For example, jQuery understands and returns the correct value for both&nbsp;.css( &quot;background-color&quot; )&nbsp;and&nbsp;.css( &quot;backgroundColor&quot; ). This means mixed case has a special meaning,&nbsp;.css( &quot;WiDtH&quot; )&nbsp;won&#39;t do the same as&nbsp;.css( &quot;width&quot; ), for example.</p><p>Note that the&nbsp;<em>computed style</em>&nbsp;of an element may not be the same as the value specified for that element in a style sheet. For example, computed styles of dimensions are almost always pixels, but they can be specified as em, ex, px or % in a style sheet. Different browsers may return CSS color values that are logically but not textually equal, e.g., #FFF, #ffffff, and rgb(255,255,255).</p><p>Retrieval of shorthand CSS properties (e.g.,&nbsp;margin,&nbsp;background,&nbsp;border), although functional with some browsers, is not guaranteed. For example, if you want to retrieve the rendered&nbsp;border-width, use:&nbsp;$( elem ).css( &quot;borderTopWidth&quot; ),&nbsp;$( elem ).css( &quot;borderBottomWidth&quot; ), and so on.</p><p>An element should be connected to the DOM when calling&nbsp;.css()&nbsp;on it. If it isn&#39;t, jQuery may throw an error.</p><p><strong>As of jQuery 1.9</strong>, passing an array of style properties to&nbsp;.css()&nbsp;will result in an object of property-value pairs. For example, to retrieve all four rendered&nbsp;border-width&nbsp;values, you could use&nbsp;$( elem ).css([ &quot;borderTopWidth&quot;, &quot;borderRightWidth&quot;, &quot;borderBottomWidth&quot;, &quot;borderLeftWidth&quot; ]).</p><p>Examples:</p><p>Get the background color of a clicked div.</p><p>1</p><p>2</p><p>3</p><p>4</p><p>5</p><p>6</p><p>7</p><p>8</p><p>9</p><p>10</p><p>11</p><p>12</p><p>13</p><p>14</p><p>15</p><p>16</p><p>17</p><p>18</p><p>19</p><p>20</p><p>21</p><p>22</p><p>23</p><p>24</p><p>25</p><p>26</p><p>27</p><p>28</p><p>29</p><p>30</p><p>31</p><p>32</p><p>33</p><p>&lt;!doctype html&gt;</p><p>&lt;html lang=&quot;en&quot;&gt;</p><p>&lt;head&gt;</p><p>&lt;meta charset=&quot;utf-8&quot;&gt;</p><p>&lt;title&gt;css demo&lt;/title&gt;</p><p>&lt;style&gt;</p><p>div {</p><p>width: 60px;</p><p>height: 60px;</p><p>margin: 5px;</p><p>float: left;</p><p>}</p><p>&lt;/style&gt;</p><p>&lt;script src=&quot;https://code.jquery.com/jquery-1.10.2.js&quot;&gt;&lt;/script&gt;</p><p>&lt;/head&gt;</p><p>&lt;body&gt;</p><p>&lt;span id=&quot;result&quot;&gt;&amp;nbsp;&lt;/span&gt;</p><p>&lt;div style=&quot;background-color:blue;&quot;&gt;&lt;/div&gt;</p><p>&lt;div style=&quot;background-color:rgb(15,99,30);&quot;&gt;&lt;/div&gt;</p><p>&lt;div style=&quot;background-color:#123456;&quot;&gt;&lt;/div&gt;</p><p>&lt;div style=&quot;background-color:#f11;&quot;&gt;&lt;/div&gt;</p><p>&lt;script&gt;</p><p>$( &quot;div&quot; ).click(function() {</p><p>var color = $( this ).css( &quot;background-color&quot; );</p><p>$( &quot;#result&quot; ).html( &quot;That div is &lt;span style=&#39;color:&quot; +</p><p>color + &quot;;&#39;&gt;&quot; + color + &quot;&lt;/span&gt;.&quot; );</p><p>});</p><p>&lt;/script&gt;</p><p>&lt;/body&gt;</p><p>&lt;/html&gt;</p>', '2017-04-26 07:58:21', 1),
(9, 'abdulsalam', '$form', '<p><strong><em>The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! &quot;Now fax quiz Jack!&quot; my brave ghost pled. Five quacking zephyrs jolt my wax bed. Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. A very bad quack might jinx zippy fowls. Few quips galvanized the mock jury box. Quick brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my wolves quack! Blowzy red vixens fight for a quick jump. Joaquin Phoenix was gazed by MTV for luck. A wizard&rsquo;s job is to vex chumps quickly in fog. Watch &quot;Jeopardy!&quot;, Alex Trebek&#39;s fun TV quiz game. Woven silk pyjamas exchanged for blue quartz. Brawny gods just flocked up to quiz and vex him. Adjusting quiver and bow, Zompyc[1] killed the fox. My faxed joke won a pager in the cable TV quiz show. Amazingly few discotheques provide jukeboxes. My girl wove six dozen plaid jackets before she quit. Six big devils from Japan quickly forgot how to waltz. Big July earthquakes confound zany experimental vow. Foxy parsons quiz and cajole the lovably dim wiki-girl. Have a pick: twenty six letters - no forcing a jumbled quiz! Crazy Fredericka bought many very exquisite opal jewels. Sixty zippers were quickly picked from the woven jute bag. A quick movement of the enemy will jeopardize six gunboats. All questions asked by five watch experts amazed the judge. Jack quietly moved up front and seized the big ball of wax.The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs! &quot;Now fax quiz Jack!&quot; my brave ghost pled. Five quacking zephyrs jolt my wax bed. Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. A very bad quack might jinx zippy fowls. Few quips galvanized the mock jury box. Quick brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my wolves quack! Blowzy red vixens fight for a quick jump. Joaquin Phoenix was gazed by MTV</em></strong></p>', '2017-04-26 08:14:06', 1),
(10, 'admin', 'Admin account credentials ', '<ul><li>Username: <strong>admin</strong></li><li>Password: <strong>adminadmin</strong></li></ul>', '2017-04-26 09:39:17', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `contactnum` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `gender`, `contactnum`, `type`) VALUES
(4, 'Nate Dogg', 'Nate@dogg.com', '$2y$10$0RKm7utIGVQfDxnS62ytd.F7NYsjQy3nxHLm.H59Xu8PVkTvMzzZ6', 'M', '69551320715', 'jpg'),
(5, 'admin', 'admin@tech.hub', '$2y$10$.iXEcFyYYP8WM97VnF7tveUZCcvYHMH6AZa.r11Y4SvJH0ZpwG2MC', 'M', '99151320715', 'jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
