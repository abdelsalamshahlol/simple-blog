<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function reg() {
        if (!$this->session->userdata('login')) {
            $data['title'] = 'Registration Page';
            $this->load->view('include/header', $data);
            $this->load->view('include/nav');
            $vals = array(
                'word' => '',
                'img_path' => './captcha/',
                'img_url' => base_url() . "captcha/",
                'font_path' => './asset/font/riffic-bold.OTF',
                'img_width' => '180',
                'img_height' => 40,
                'expiration' => 1000,
                'word_length' => 3,
                'font_size' => 12,
                'img_id' => 'Imageid',
                'pool' => '!@#$%^&*0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                // White background and border, black text and red grid
                'colors' => array(
                    'background' => array(255, 255, 255),
                    'border' => array(224, 226, 229),
                    'text' => array(22, 163, 35),
                    'grid' => array(255, 250, 255)
                )
            );
            $cap = create_captcha($vals);
            $this->session->set_userdata(array('captcha' => $cap['word']));
            $this->load->view('user/regpage', $cap);
            $this->load->view('include/footer');
        } else {
            redirect(base_url());
        }
    }

    public function adduser() {
        $form = $this->input->post();
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[5]|max_length[12]|is_unique[users.username]');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|min_length[8]|required', array('required' => 'You must provide a %s.')
        );
        $this->form_validation->set_rules('passcon', 'Password Confirmation', 'trim|required|matches[password]');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_rules('contactnum', 'Contact Number', 'is_unique[users.contactnum]|numeric|trim|required|min_length[10]|max_length[15]');
        $this->form_validation->set_rules('userCaptcha', 'Captcha', 'required|callback_validate_captcha');
        $this->form_validation->set_rules('userfile', 'Profile picture', 'callback_file_check');

        if ($this->form_validation->run() == FALSE) {
            $this->reg();
        } else {
            $this->do_upload($form);
            echo $this->upload->display_errors('<p>', '</p>');
            $tmp = explode('.', $_FILES['userfile']['name']);
            $type = end($tmp);
            $form['type'] = $type;
            unset($form['userfile']);
            unset($form['userCaptcha']);
            unset($form['passcon']);
            $form['password'] = password_hash($form['password'], PASSWORD_DEFAULT);
            $this->User_model->insertdata($form);
            redirect(base_url()).'login';
        }
    }

    public function verifylogin() {
        $usercred = $this->input->post();
        $userdb = $this->User_model->check($usercred);
        $userdbpass = trim($userdb[0]['password']); //trim is very critical !!!
        if (password_verify($usercred['password'], $userdbpass)) {
            $sess_data = array('login' => TRUE, 'username' => $userdb[0]['username'], 'id' => $userdb[0]['id']);
            $this->session->set_userdata($sess_data);
            redirect(base_url());
        } else {
            $this->session->set_flashdata('msg', 'Wrong Username or Password!');
            redirect(base_url() . 'login');
        }
    }

    public function do_upload($form_data) {
//        print_r($form_data);
        $config2['upload_path'] = './userdp/';
//this is not working even if the picture is png its not allowing it, Validation is done with my custom callback
//so config for type and size is commented 
//        $config2['allowed_types'] = 'gif|jpg|png';
        $config2['allowed_types'] = "*";
//        $config2['max_size'] = 2048;
//        $config2['max_width'] = 8000;
//        $config2['max_height'] = 8000;
        $config2['file_name'] = $form_data['contactnum'];
        $config2['overwrite'] = TRUE;

        $this->load->library('upload', $config2);

        $this->upload->do_upload();
        $data = $this->upload->data();
        $config['image_library'] = 'gd2';
        $config['source_image'] = './userdp/' . $data['file_name'];
        $config['new_image'] = './userdp_thumb/' . $data['file_name'];
        $config['create_thumb'] = TRUE;
        $config['thumb_marker'] = "";
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 200;
        $config['height'] = 200;

        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

    /* This is a custom call back to for validation. it will check if the file uploaded is valid as pic or not 
     * and to the size of it.
     * This validation won't fail ever so the users data won't be insterted in the database.
     * sometimes the file upload limit causes errors and data mighht be submitted using the
     * standard validation way provided by ci */

    public function file_check($str) {
        $allowed_mime_type_arr = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png');
        $mime = get_mime_by_extension($_FILES['userfile']['name']);
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != "") {
            if (in_array($mime, $allowed_mime_type_arr)) {
                $filename = $_FILES['userfile']['tmp_name'];
                list($width, $height) = getimagesize($filename);
                if ($width < 8000 && $height < 8000 && $_FILES['userfile']['size'] <= 3545728) {
                    return true;
                } else {
                    $this->form_validation->set_message('file_check', 'File is larger than limit of 3 Megabytes.');
                    return false;
                }
            } else {
                $this->form_validation->set_message('file_check', 'Please select only gif/jpg/png file.');
                return false;
            }
        } else {
            $this->form_validation->set_message('file_check', 'Please choose a file to upload.');
            return false;
        }
    }

    public function validate_captcha() {
        if ($this->input->post('userCaptcha') != $this->session->userdata['captcha']) {
            $this->form_validation->set_message('validate_captcha', 'Wrong captcha code, hmm are you the Terminator?');
            return false;
        } else {
            return true;
        }
    }

    public function savepost() {
        $form = $this->input->post();
        $this->form_validation->set_rules('title', 'Post Title', 'trim|required|min_length[3]|max_length[50]', array('required' => ' Give us a title if you want your post to be published !'));
        $this->form_validation->set_rules('editor1', 'Blog post', 'required|trim|min_length[3]|max_length[999999]', array('required' => 'Write something first !'));

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url() . 'writepost');
        } else {
            $user_post = array('user_id' => 1,
                'username' => $_SESSION['username'],
                'title' => $form['title'],
                'text' => $form['editor1']);
            $this->User_model->insertpost($user_post);
            redirect(base_url());
        }
    }

    public function savecomment() {
        $form = $this->input->post();
        $this->form_validation->set_rules('title', 'Comment Title', 'trim|required|min_length[3]|max_length[50]', array('required' => ' Give us a title if you want your post to be published !'));
        $this->form_validation->set_rules('editor1', 'Blog post', 'required|trim|min_length[3]|max_length[999999]', array('required' => 'Write something first !'));

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url() . 'post/' . $form['post_id']);
        } else {
            $user_post = array('user_id' => 1,
                'username' => $_SESSION['username'],
                'title' => $form['title'],
                'comment' => $form['editor1'],
                'post_id' => $form['post_id']);
            $this->User_model->insertcomment($user_post);
            redirect(base_url() . 'post/' . $form['post_id']);
        }
    }

    public function message() {
        $user = isset($_SESSION['username']) ? $_SESSION['username'] : "";
        $data["flashdata"] = 'See you next time ' . $user;
        if ($this->session->userdata('login')) {
            $this->load->view('include/header', $data);
            $this->load->view('user/message', $data);
            $this->load->view('include/header', $data);
        } else {
            redirect(base_url());
        }
    }

}
