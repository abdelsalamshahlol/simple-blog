<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index() {
        $data['title'] = 'Tech Hub';
        $this->load->view('include/header', $data);
        $this->load->view('include/nav');
        $data['posts'] = $this->User_model->getposts();
        $this->load->view('blog/home', $data);
        $this->load->view('include/footer');
    }

    public function login() {
        if (!$this->session->userdata('login')) {
            $data['title'] = 'User Login to Tech Hub';
            $this->load->view('include/header', $data);
            $this->load->view('user/loginpage');
        } else {
            redirect(base_url() . 'users/message');
        }
    }

    public function writepost() {
        if ($this->session->userdata('login')) {
            $data['title'] = 'Write Post';
            $this->load->view('include/header', $data);
            $this->load->view('include/nav');
            $this->load->view('blog/addpost');
            $this->load->view('include/footer');
        } else {
            redirect(base_url());
        }
    }

    public function contread($id) {
        $data['post'] = $this->User_model->getpost($id);
        $data['comments'] = $this->User_model->getcomments($id);
        $data['title'] = $data['post'][0]['title'];
        $this->load->view('include/header', $data);
        $this->load->view('include/nav');
        $this->load->view('blog/post', $data);
        $this->load->view('include/footer');
    }

}
