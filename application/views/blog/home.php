<style>
    #posts{
        /*margin-bottom: 70%;*/
        padding-top: 10px;
        padding-bottom: 20px;
    }
</style>
<script>
    $(document).ready(function () {
        var error = "<?= $this->session->flashdata('later') ?>";
        if (error != "") {
            $("#a1").show();
            function toggleDiv() {
                setTimeout(function () {
                    $("#a1").fadeOut("slow");
                }, 5000);
            }
            toggleDiv();
        }
    }
    );
</script>
<div style="position: absolute; display: none;" class="alert alert-danger text-center col-md-12" id="a1">
    <?php echo $this->session->flashdata('later'); ?>
</div>

<h2 class="text-capitalize text-info text-center">Welcome to Tech Hub Blog</h2>
<div  class="">
    <?php foreach ($posts as $p): ?>
        <!--<div class="row">-->
        <div id='posts' class="col-md-8 col-md-offset-2">
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <a href="<?= base_url() . 'post/' . $p['id'] ?>"><span class="text-capitalize text-left"><strong><?= $p['title'] ?></strong></span></a>
                    <span class="text-capitalize text-left">Published by <a href="#"><?= $p['username'] ?></a></span>
                    <div class="pull-right"><span class="text-capitalize">on </span><time><strong><?= $p['datepublished'] ?></strong></time></div>
                </div>
                <div class="panel-body">
                    <?php
                    $string = $p['text'];
                    $string = strip_tags($string);
                    if (strlen($string) > 500) {
                        $stringCut = substr($string, 0, 500);
                        $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '... <a href=' . base_url() . "post/" . $p['id'] . '>Read More</a>';
                    }
                    echo $string;
                    ?>
                </div>
            </div>
        </div>
        <!--</div>-->
    <?php endforeach; ?>
</div>

