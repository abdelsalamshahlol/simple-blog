<script src="//cdn.ckeditor.com/4.6.2/basic/ckeditor.js"></script>
<h4 class="text-capitalize text-center">Use the text editor to add write a new post. Click <b>Cancel</b> to redirect to home or Publish to save post. </h4>
<br>
<div style="position: fixed; display: none;" class="alert alert-danger text-center col-md-6 col-md-offset-3" id="a1">
    <?php echo $this->session->flashdata('errors'); ?>
</div>
<br>
<br>
<br>
<form method="post" action="<?= base_url() ?>users/savepost" name="userpost">
    <div  style="visibility: hidden" id="text-editor">
        <div class="form-group row col-md-8 col-md-offset-2">
            <label class=" control-label" for="title">Title:</label>  
            <input  name="title" type="text" placeholder="" class="form-control input-md">
        </div>
        <div class="form-group">
            <div class="container col-md-8 col-md-offset-2">
                <label class=" control-label">Post:</label>  
                <textarea class="hidden" name="editor1"></textarea>
                <br>
                <button id="btn1" class="btn btn-primary">Publish Post</button>
                <a href="<?= base_url() ?>"><input type="button" value="Cancel" id="btn2" class="btn btn-default"></a>
            </div>
        </div>
    </div>
</form>


<script>
    $(document).ready(function () {
        var error = <?php echo json_encode($this->session->flashdata('errors'))?>;
        if (error !== "" && error!==null) {
            $("#a1").show();
            function toggleDiv() {
                setTimeout(function () {
                    $("#a1").fadeOut("slow");
                }, 5000);
            }
            toggleDiv();
        }
    }
    );

    CKEDITOR.replace('editor1', {
        on: {
            instanceReady: function (evt) {
                // your stuff here
                $("#text-editor").css("visibility", 'visible')//.fadeIn(900);
//                $("#text-editor").hide().fadeIn(0);
                $("#text-editor").hide().fadeIn(800);
            }
        }
    });
</script>
