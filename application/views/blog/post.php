<style>
    #comm-sec{
        margin-bottom: 100px;
    }
</style>

<script src="//cdn.ckeditor.com/4.6.2/basic/ckeditor.js"></script>
<div class="col-md-10 col-md-offset-1">
    <div class="conatiner">
        <div class="head">
            <h1 class=" "><strong><?= $post[0]['title'] ?></strong></h1>
            <span class="text-capitalize text-left">Published by <a href="#"><?= $post[0]['username'] ?></a></span>
            <span class="text-capitalize">on </span><time class="meta-date"><strong><?= $post[0]['datepublished'] ?></strong></time>
        </div><br>
        <div class="">
            <p class="text">
                <?= $post[0]['text'] ?>
            </p>
        </div>
    </div>
    
    <div class="row" id='comm-sec'>
        <h3>Comments:</h3>

        <?php foreach ($comments as $c): ?>
            <!--<div class="row">-->
            <div class="col-md-12">
                <div class="panel panel-default ">
                    <div class="panel-heading">
                        <span class="text-capitalize text-left"><strong><?= $c['title'] ?></strong></span>
                        <span class="text-capitalize text-left">Comment by user <a href="#"><?= $c['username'] ?></a></span>
                        <div class="pull-right"><span class="text-capitalize">on </span><time><strong><?= $c['date_written'] ?></strong></time></div>
                    </div>
                    <div class="panel-body">

                        <?= $c['comment']; ?>
                    </div>
                </div>
            </div>
            <!--</div>-->
        <?php endforeach; ?>

<!--        <h4 class="text-capitalize text-center">Use the text editor to add write a new post. Click <b>Cancel</b> to redirect to home or Publish to save post. </h4>-->
        <br>
        <div style="position: fixed; display: none;" class="alert alert-danger text-center col-md-6 col-md-offset-3" id="a1">
            <?php echo $this->session->flashdata('errors'); ?>
        </div>
        <br>
        <br>
        <br>
        <form method="post" action="<?= base_url() ?>users/savecomment" name="userpost">
            <input hidden value="<?= $this->uri->segment(2) ?>" name="post_id">
            <div class='container col-md-12' style="visibility: hidden" id="text-editor">
                <div class="form-group">
                    <label class=" control-label" for="title">Title:</label>  
                    <input  name="title" type="text" placeholder="" class="form-control input-md">
                </div>
                <div class="form-group">
                    <div class="">
                        <label class=" control-label">Post:</label>  
                        <textarea class="hidden" id='editor1' name="editor1"></textarea>
                        <br>
                        <button id="btn1" class="btn btn-primary">Publish Comment</button>
                        <a href="<?= base_url() ?>"><input type="button" value="Cancel" id="btn2" class="btn btn-default"></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var userlogged="<?=$this->session->userdata('login')?>";
    if(userlogged==1){
         CKEDITOR.replace('editor1', {
        on: {
            instanceReady: function (evt) {
                // your stuff here
                $("#text-editor").css("visibility", 'visible')//.fadeIn(900);
                //                $("#text-editor").hide().fadeIn(0);
                $("#text-editor").hide().fadeIn(800);
            }
        }
    });
    }else{
//        $('#text-editor').hide();
    }
    $(document).ready(function () {
        var error = <?php echo json_encode($this->session->flashdata('errors')) ?>;
        if (error !== "" && error !== null) {
            $("#a1").show();
            function toggleDiv() {
                setTimeout(function () {
                    $("#a1").fadeOut("slow");
                }, 5000);
            }
            toggleDiv();
        }
    }
    );

   
</script>
