<style>
    body{
        background: #1e3c72;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #2a5298, #1e3c72);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #2a5298, #1e3c72); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }
    .vertical-offset-100{
        padding-top:100px;
    }
</style>

<script>
    $(document).ready(function () {
        var error = "<?= $this->session->flashdata('msg') ?>";
        if (error != "") {
            $("#a1").show();
            function toggleDiv() {
                setTimeout(function () {
                    $("#a1").fadeOut("slow");
                }, 5000);
            }
            toggleDiv();
        }
    }
    );
</script>
<div style="position: absolute; display: none;" class="alert alert-danger text-center col-md-12" id="a1">
    <?php echo $this->session->flashdata('msg'); ?>
</div>

<div class="container">
    <div class="row vertical-offset-100">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please sign in</h3>
                </div>
                <div class="panel-body">
                    <form name="usercred" accept-charset="UTF-8" method="post" action="<?= base_url() ?>users/verifylogin" role="form">
                        <fieldset>
                            <div class="form-group">
                                <label class="label">Username</label>
                                <input class="form-control" placeholder="Username" name="username" type="text">
                            </div>
                            <div class="form-group">
                                <label class="label ">Password</label>
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember me"> Remember me.
                                </label>
                            </div>
                            <input class="btn btn-lg btn-primary btn-block" type="submit" value="Login">
                        </fieldset>
                    </form>
                </div>                         
            </div>
        </div>
    </div>
</div>