<style>
    #form1{
        margin-bottom: 150px;
    }
</style>


<h2 class="text-center">Sign up with Tech Hub to get access to write and comment on posts.</h2>
<form id='form1' class="form-horizontal" name="newuser" action="<?= base_url() ?>users/adduser" method="post" enctype="multipart/form-data">
    <fieldset class="container">
        <div class="panel panel-body">
            <!-- Form Name -->
            <legend>Fill all the following requirement:</legend>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="userame">Name</label>  
                <div class="col-md-3">
                    <input id="Name" name="username" type="text" placeholder="" class="form-control input-md">

                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>
                <div class="col-md-3">
                    <input id="password" name="password" type="password" placeholder="" class="form-control input-md">
                </div>
            </div>

            <!-- Confirmation Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="passcon">Password Confirmation</label>
                <div class="col-md-3">
                    <input id="passcon" name="passcon" type="password" placeholder="" class="form-control input-md">
                </div>
            </div>


            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="email">Email</label>  
                <div class="col-md-3">
                    <input id="email" name="email" type="text" placeholder="" class="form-control input-md">

                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="number">Contact Number</label>  
                <div class="col-md-3">
                    <input id="number" name="contactnum" type="text" placeholder="" class="form-control input-md">

                </div>
            </div>


            <!-- Select Basic -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="gender">Gender</label>
                <div class="col-md-3">
                    <select id="gender" name="gender" class="form-control">
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
            </div>

            <!-- Profile picture --> 
            <?php echo form_open_multipart(base_url() . 'users/do_upload'); ?>
            <div class="form-group">
                <label class="col-md-4 control-label" for="filebutton">Profile Picture</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <input type="text" class="form-control" id="filename">
                        <label class="btn btn-default input-group-addon" id="uploadFile" for="UploadField">
                            <span class="glyphicon glyphicon-upload">
                                <input id="UploadField"  type="file" style="display:none;" name="userfile" size="20" >
                            </span> Choose File</label>
                    </div>
                            <!--<input id="filebutton" name="userimage" class="input-file" type="file">-->
                </div>
            </div>
            <!--</form>-->

            <!-- Text input-->
            <div class="form-group ">
                <label class=" control-label col-md-4" for="cap">CAPTCHA</label> 
                <label for="captcha" style="width:0px"><?php echo $image; ?></label>
                <div class="col-md-3">
                    <input id="cap"  name="userCaptcha" type="text" placeholder="" class="form-control input-md ">
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <!--<label class="col-md-4 control-label" for="singlebutton">Single Button</label>-->
                <div class="col-md-4 col-md-offset-4">
                    <button id="singlebutton" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </fieldset>
</form>
<div class="alert alert-danger container <?php echo "", (validation_errors() != "" ? 'show' : 'hidden'); ?>">
    <?= validation_errors() ?>
</div>
<script>
    $(function () {
//        $('.error_msg').hide();
//        $('.editModal').on('hidden.bs.modal', (function () {
//            $('.error_msg').hide();
//            $('.error_msg').html("");
//        }));

        //function to set file name of edit modal name label
        $('#uploadFile').on('click', function () {
            $('#UploadField').change(function () {
                var file = $('#UploadField').val().split('\\').pop();
                $('#filename').val(file);
            });
        });
    });
</script>

