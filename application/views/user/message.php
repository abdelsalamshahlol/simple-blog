<script>
    $('body').addClass('jumbotron');
</script>
<div class="">
    <div class=" col-md-6 col-md-offset-3">
        <h1 class="text-capitalize"><?php
            echo $flashdata;
            ?>
        </h1>
    </div>
</div>

<?php
session_destroy();
header("refresh:5; url=" . base_url());

