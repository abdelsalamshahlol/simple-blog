<div class="navbar-static-top">
   <nav class="navbar navbar-default">
      <div class="container-fluid">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= base_url() ?>">Tech Hub Blog</a>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
               <li class="<?php if($this->uri->segment(2)=="55"){echo 'active';}?>"><a href="<?= base_url() ?>">Home <span class="sr-only">(current)</span></a></li>
               <li class="<?php if($this->uri->segment(1)==("writepost")){echo 'active';}?>"><a href="<?= base_url() ?>writepost">Write Post</a></li>
               <!--<li class="<?php if($this->uri->segment(2)==("adduser")){echo 'active';}?>"><a href="<?= base_url() ?>">Add User</a></li>-->
               <li class="<?php if($this->uri->segment(2)==("register")){echo 'active';}?>"><a href="<?= base_url() ?>users/register">Sign Up</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden-xs"><a href="#"><span class="text-capitalize"><?php echo "" ,isset($this->session->userdata['username'])?"Welcome back ".$this->session->userdata['username']:""?></span></a></li>
               <li><a href="<?=base_url()?>login">Login /Logout</a></li>
            </ul>
         </div>
         <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
   </nav>
</div>