<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function getpost($id) {
        $this->db->select('*');
        $this->db->where('id',$id);
        $q = $this->db->get('posts')->result_array();  // Produces: SELECT * FROM mytable
        return $q;
    }
    
    public function getcomments($id) {
        $this->db->select('*');
        $this->db->where('post_id',$id);
        $q = $this->db->get('comments')->result_array();
        return $q;
    }

    public function check($usercred) {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where(array('username' => $usercred['username'])); //, 'pass' => $usercred['password']));
        $this->db->limit(1);
        $query = $this->db->get()->result_array();
        return $query;
//        return $query->num_rows()?TRUE:FALSE;
    }

    public function insertdata($data) {
        $this->db->insert('users', $data);
    }

    public function insertpost($data) {
        $this->db->insert('posts', $data);
    }
    public function insertcomment($data) {
        $this->db->insert('comments', $data);
    }

    public function getposts() {
        $this->db->from('posts');
		$this->db->order_by('datepublished','DESC');
        $posts = $this->db->get()->result_array();
        return $posts;
    }

}
